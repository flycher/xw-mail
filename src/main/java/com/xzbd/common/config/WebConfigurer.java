package com.xzbd.common.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 *  本地虚拟路径映射
 *  如实现的配置是把有关 `/files/**` 的资源请求，
 *  转发到本地 `file:///D:/z_workspace/z_temp/` 下
 *  寻找资源。该配置可以实现访问本地资源，如访问
 *  z_tmp目录下的图片、excel文件等
 */
@Component
class WebConfigurer extends WebMvcConfigurerAdapter {
	@Autowired
	XwMailConfig xwMailConfig;
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/files/**").addResourceLocations("file:///"+xwMailConfig.getUploadPath());
	}

}